# sample-ibm

To test the feasibility of gitlab

--- | Azure | OpenShift | GitLab
--- | --- | --- | ---
Source | Open | Open | Open
SCM (git)<sup>1</sup> | Azure Repos | gogs | GitLab
Issue tracking |Azure tracker - | - | GitLab tracker
Artifacts |Azure artifacts - | - | Gitlab
CI/CD      |Azure pipeline - | - | Gitlab CI
Monitor |  - | Jenkins | - Gitlab Self monitoring
Automation server |  - | Jenkins | - Gitlab runner
Automation script | - | bash, maven | -